const API = 'https://jsonplaceholder.typicode.com/comments?_start=0&_limit=50';
export default function Users(props) {
  return (
    <>
      {console.log(props)}
      <div className="container flex h-full w-full items-center flex-col mx-auto">
        <h1 className="font-bold text-4xl">USERS </h1>
        <div className="mt-10 w-full">
          <div className="grid grid-cols-4 text-center">
            {' '}
            <p>No</p>
            <h2>name</h2>
            <h2>email</h2>
            <h2>body</h2>
          </div>
          {props.posts?.map((post) => (
            <div key={post.id} className="grid grid-cols-4 text-center py-2">
              <p>{post.id}</p>
              <h2>{post.name}</h2>
              <h2>{post.email}</h2>
              <h2>{post.body}</h2>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

export async function getServerSideProps(context) {
  const res = await fetch(API);
  const posts = await res.json();
  return {
    props: { posts },
  };
}
